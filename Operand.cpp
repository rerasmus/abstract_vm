/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Operand.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rerasmus   <marvin@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/27 10/43/59 by rerasmus          #+#    #+#             */
/*   Updated: 2018/07/13 15:12:29 by rerasmus         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Operand.hpp"

Operand::Operand() {}

Operand::Operand(eOperandType type, std::string value): _type(type), 
	_value(value){
		std::cout << "Operand Constructed()" << std::endl;

		switch (type){
			case Int8:
				this->_precision = 0;
				break;
			case Int16:
				this->_precision = 1;
				break;
			case Int32:
				this->_precision = 2;
				break;
			case Float:
				this->_precision = 8;
				break;
			case Double:
				this->_precision = 16;
				break;
		}

		std::cout << "Precision: " << this->_precision << std::endl;
	}

Operand::Operand(Operand const &src) {
	*this = src;	
}

Operand::~Operand() {
	std::cout << "Operand Destructed()" << std::endl;
}

Operand				&Operand::operator=(Operand const &src) {
	if (this != &src){
		*this = src;
	}
	return (*this);
}

//GETTERS

int					Operand::getPrecision() const{
	return this->_precision;
}

eOperandType		Operand::getType(void) const{
	return this->_type;
}

//FUNCTIONS

IOperand const		*Operand::operator+(IOperand const &rhs) const{
	std::cout << "+ Operator Called" << std::endl;
	
	std::string			str;
	std::stringstream	conv;
	long double			a = std::stold(this->_value);
	std::cout << "+) a: " << a << std::endl;
	long double			b = std::stold(rhs.toString());
	std::cout << "+) b: " << b << std::endl;
	long double			ans = a+b;

	eOperandType		type = this->getPrecision() > rhs.getPrecision() ? this->getType() : rhs.getType();

	return new Operand(type, std::to_string(ans));
}

IOperand const		*Operand::operator-(IOperand const &rhs) const{
	std::cout << "- Operator Called" << std::endl;

	long double a = std::stold(this->_value);
	std::cout << "-) a: " << a << std::endl;
	long double b = std::stold(rhs.toString());
	std::cout << "-) b: " << b << std::endl;

	long double ans = a-b;
	eOperandType type = this->getPrecision() > rhs.getPrecision() ? this->getType() : rhs.getType();

	return new Operand(type, std::to_string(ans));
}

IOperand const		*Operand::operator*(IOperand const &rhs) const{
	std::cout << "* Operator Called" << std::endl;

	long double a = std::stold(this->_value);
	std::cout << "*) a: " << a << std::endl;
	long double b = std::stold(rhs.toString());
	std::cout << "*) b: " << b << std::endl;

	long double ans = a*b;
	eOperandType type = this->getPrecision() > rhs.getPrecision() ? this->getType() : rhs.getType();

	return new Operand(type, std::to_string(ans));
}

IOperand const		*Operand::operator/(IOperand const &rhs) const{
	std::cout << "/ Operator Called" << std::endl;

	long double a = std::stold(this->_value);
	std::cout << "/) a: " << a << std::endl;
	long double b = std::stold(rhs.toString());
	std::cout << "/) b: " << b << std::endl;

	if (a == 0)
		throw DivZeroException();

	long double ans = b/a;
	eOperandType type = this->getPrecision() > rhs.getPrecision() ? this->getType() : rhs.getType();

	return new Operand(type, std::to_string(ans));
}

IOperand const		*Operand::operator%(IOperand const &rhs) const{
	std::cout << "mod Operator Called" << std::endl;

	long double a = std::stold(this->_value);
	std::cout << "%) a: " << a << std::endl;
	long double b = std::stold(rhs.toString());
	std::cout << "%) b: " << b << std::endl;

	if (a == 0)
		throw ModZeroException();

	long double ans = std::fmod(b,a);
	eOperandType type = this->getPrecision() > rhs.getPrecision() ? this->getType() : rhs.getType();

	return new Operand(type, std::to_string(ans));
}

std::string const	&Operand::toString(void) const{
	return this->_value;
}

